//
//  JobCellViewModel.swift
//  Pratice104
//
//  Created by Cliff Chen on 2022/2/21.
//

import Foundation
import SwiftyJSON

class JobCellViewModel {
    var name: String
    var job: String
    var address: String
    var salary: String
    var upDateTime: String
    
    init(jobData: JSON) {
        self.job = jobData["JOB"].string ?? ""
        self.name = jobData["NAME"].string ?? ""
        self.address = jobData["ADDR_NO_DESCRIPT"].string ?? ""
        
        // 薪水判斷
        let lowSal = jobData["SAL_MONTH_LOW"].int ?? 0
        let highSal = jobData["SAL_MONTH_HIGH"].int ?? 0
        if lowSal == 0 {
            self.salary = "待遇面議"
        } else {
            self.salary = "月薪\(lowSal/10000)萬-\(highSal/10000)萬元"
        }
        
        // 日期轉換
        let appearDate = jobData["APPEAR_DATE"].string ?? "0101"
        var dayString = appearDate.suffix(4)
        dayString.insert("-", at: dayString.index(dayString.startIndex, offsetBy: 2))
        self.upDateTime = String(dayString)
        
    }
    
}
