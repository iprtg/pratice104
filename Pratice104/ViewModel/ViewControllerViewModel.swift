//
//  ViewModel.swift
//  Pratice104
//
//  Created by Cliff Chen on 2022/2/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class ViewControllerViewModel: NSObject {
    
    // MARK: variable
    var jobDataArray: [JSON] = []
    var jobCellViewModels: [JobCellViewModel] = []
    var onRequestEnd: (() -> Void)?
    
    func getJobData() {
        struct DecodableType: Decodable { let url: String }
        let parameters = [
            "page" : "1",
            "pgsz" : "50",
            "fmt" : "8",
            "cols" : "JOB,NAME,ADDR_NO_DESCRIPT,JOB_ADDR_NO_DESCRIPT,SAL_MONTH_HIGH,SAL_MONTH_LOW,APPEAR_DATE"
        ]
        AF.request("https://www.104.com.tw/i/apis/jobsearch.cfm",parameters: parameters).responseData { response in
            switch response.result {
            case .success(let value):
                debugPrint("Validation Successful")
                let json = JSON(value)
                self.jobDataArray = json["data"].array ?? []
                self.convertJobToViewModel(jobs: self.jobDataArray)
                debugPrint(json)
            case let .failure(error):
                debugPrint(error)
            }
        }
        
    }
    
    func convertJobToViewModel(jobs: [JSON]) {
        jobCellViewModels = []
        for job in jobs {
            let jobCellModel = JobCellViewModel(jobData: job)
            jobCellViewModels.append(jobCellModel)
        }
        onRequestEnd?()
    }
}
