//
//  ViewController.swift
//  Pratice104
//
//  Created by Cliff Chen on 2022/2/20.
//

import UIKit
import ImageSlideshow
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: IBOutelet
    @IBOutlet weak var searBar: UISearchBar!
    
    @IBOutlet weak var adSlideShowView: ImageSlideshow!
    @IBOutlet weak var qaSlideShowView: ImageSlideshow!
    
    @IBOutlet weak var jobListTableView: UITableView!
    
    @IBOutlet weak var btnType1: UIButton!
    @IBOutlet weak var btnType2: UIButton!
    @IBOutlet weak var btnType3: UIButton!
    @IBOutlet weak var btnType4: UIButton!
    
    @IBOutlet weak var btnListType1: UIButton! // 推薦
    @IBOutlet weak var btnListType2: UIButton! // 最新
    
    // MARK: IBAction
    
    @IBAction func touchListType1(_ sender: Any) {
        btnListType1.setTitleColor(.black, for: .normal)
        btnListType2.setTitleColor(.darkGray, for: .normal)
    }
    
    @IBAction func touchListType2(_ sender: Any) {
        btnListType1.setTitleColor(.darkGray, for: .normal)
        btnListType2.setTitleColor(.black, for: .normal)
    }

    // MARK: -- Variable
    
    let viewModel = ViewControllerViewModel()
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setUpUI()
        self.bindViewModel()
        viewModel.getJobData()
    }
    
    override func viewDidLayoutSubviews() {
        self.setUpSearchBar()
    }
    
    // MARK: -- ViewFunction
    
    private func setUpUI() {
        self.initialADSlideShow()
        self.initialqaSlideShow()
        self.intitialButtonType()
        self.initialRefrsh()
    }
    
    private func initialADSlideShow() {
        adSlideShowView.contentScaleMode = .scaleAspectFill
        /*
         adSlideShowView.pageIndicator可以改成custome indicator，有indicator image就可以改成想要的樣式
         */
        adSlideShowView.setImageInputs([
            // 可以補一張defalut image, 圖片路徑出錯時做為預設圖片
            ImageSource(image: UIImage(named: "ad1.jpeg")!),
            ImageSource(image: UIImage(named: "ad2.jpeg")!),
            ImageSource(image: UIImage(named: "ad3.jpeg")!)
        ])
    }
    
    private func initialqaSlideShow() {
        qaSlideShowView.contentScaleMode = .scaleAspectFill
        qaSlideShowView.setImageInputs([
            // 可以補一張defalut image, 圖片路徑出錯時做為預設圖片
            ImageSource(image: UIImage(named: "qa1.jpg")!),
            ImageSource(image: UIImage(named: "qa2.jpg")!)
        ])
    }
    
    private func intitialButtonType() {
        setButtonToRound(targetBtn: btnType1)
        setButtonToRound(targetBtn: btnType2)
        setButtonToRound(targetBtn: btnType3)
        setButtonToRound(targetBtn: btnType4)
    }
    
    private func setButtonToRound(targetBtn: UIButton) {
        targetBtn.imageView?.layer.cornerRadius = (targetBtn.imageView?.image?.size.width ?? 30) / 2
        targetBtn.imageView?.layer.masksToBounds = true
    }
    
    private func initialRefrsh() {
        refreshControl = UIRefreshControl()
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        refreshControl.attributedTitle = NSAttributedString(string: "Release to refresh", attributes: attributes)
        refreshControl.tintColor = UIColor.white
        refreshControl.backgroundColor = UIColor.systemGray5
        self.jobListTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(dropDownToRefresh), for: UIControl.Event.valueChanged)
    }
    
    private func setUpSearchBar() {
        searBar.setTextField(color: UIColor.white)
        searBar.setPlaceholder(textColor: .orange)
    }
    
    // MARK: -- SelfFunction
    
    func bindViewModel() {
        viewModel.onRequestEnd = { [weak self] in
            DispatchQueue.main.async {
                self?.jobListTableView.reloadData()
                self?.refreshControl!.endRefreshing()
            }
        }
    }
    
    @objc func dropDownToRefresh() {
        viewModel.getJobData()
    }
    
    // MARK: -- TableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.jobCellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "jobListCell", for: indexPath) as? JobTableViewCell else {
            let cell = UITableViewCell()
            return cell
        }
        let jobCellViewModel = viewModel.jobCellViewModels[indexPath.row]
        cell.setup(viewModel: jobCellViewModel)
        
        return cell
    }
    
}

