//
//  JobTableViewCell.swift
//  Pratice104
//
//  Created by Cliff Chen on 2022/2/21.
//

import UIKit

class JobTableViewCell: UITableViewCell {

    @IBOutlet weak var labJob: UILabel!
    @IBOutlet weak var labName: UILabel!
    @IBOutlet weak var labSalary: UILabel!
    @IBOutlet weak var labExperience: UILabel!
    @IBOutlet weak var labAddr: UILabel!
    @IBOutlet weak var labUpdateTime: UILabel!
    
    private var viewModel: JobCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setup(viewModel: JobCellViewModel) {
        self.viewModel = viewModel
        
        self.labJob.text = viewModel.job
        self.labName.text = viewModel.name
        self.labAddr.text = viewModel.address
        self.labSalary.text = viewModel.salary
        self.labUpdateTime.text = viewModel.upDateTime
    }
}
